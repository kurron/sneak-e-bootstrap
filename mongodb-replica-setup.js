replicaConfig = {
    _id: 'mold-e',
    members: [
        {_id: 1, host: '192.168.33.10'},
        {_id: 2, host: '192.168.33.20'},
        {_id: 3, host: '192.168.33.60'}
    ]
}

rs.initiate( replicaConfig )
