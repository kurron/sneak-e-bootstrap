#!/usr/bin/env bash

DONEFILE=/var/vagrant-rabbitmq

# make sure we are idempotent
if [ -f "${DONEFILE}" ]; then
    exit 0
fi

# Actual shell commands here.
echo "Installing RabbitMQ Server..."
cd /tmp ; wget http://www.rabbitmq.com/rabbitmq-signing-key-public.asc ; apt-key add rabbitmq-signing-key-public.asc
echo 'deb http://www.rabbitmq.com/debian/ testing main' | tee /etc/apt/sources.list.d/rabbitmq.list
aptitude update
aptitude install -y rabbitmq-server 
rabbitmq-plugins enable rabbitmq_management
rabbitmq-plugins enable rabbitmq_consistent_hash_exchange 
rabbitmq-plugins enable rabbitmq_federation 
rabbitmq-plugins enable rabbitmq_shovel 
rabbitmq-plugins enable rabbitmq_stomp 
rabbitmq-plugins enable rabbitmq_tracing 
rabbitmq-plugins list
/etc/init.d/rabbitmq-server restart

# signal a successful provision
touch ${DONEFILE}
