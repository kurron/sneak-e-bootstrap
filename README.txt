* install Vagrant (vagrantup.com)
* install VirtualBox (virtualbox.org)
* vagrant up (make sure to wait for the script to finish running before accessing the IDE VM)
* vagrant status (will show the status of the boxes)
* enjoy your elf-contained environment 
* configured for the Mold-E project (https://svn.transparent.com/repos/TLShared/Java/Projects/Mold-E/)
