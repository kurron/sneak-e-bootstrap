#!/usr/bin/env bash

DONEFILE=/var/vagrant-chrome

# make sure we are idempotent
if [ -f "${DONEFILE}" ]; then
    exit 0
fi

# Actual shell commands here.
echo "Installing Chromium Browser..."
aptitude install -y chromium-browser 

# signal a successful provision
touch ${DONEFILE}
